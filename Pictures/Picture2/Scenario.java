//=================================================
//Snowman scene 2.
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Scenario extends JApplet {

 public void paint(Graphics page) {
        final int WIDTH = 600;
        final int HEIGHT = 400;

        //fill entire background cyan
        page.setColor(Color.cyan);
        page.fillRect(0,0,WIDTH, HEIGHT);

        //draw sun in corner
        page.setColor(Color.yellow);
     page.fillOval(0,0,50,50);    // Graphics Object #2: Oval
      //draw rays from sun
    page.drawLine(70,30,100,50); // Graphics Object #3: Line
    page.drawLine(30,70,50,100);
    page.drawLine(50,50,100,100);
     page.drawLine(70,10,150,15);
     page.drawLine(10,65,6,130);

	//snow on ground
	page.setColor(Color.white);
	 page.fillRect(0,HEIGHT - 80,WIDTH,HEIGHT);

     //Snowman

	//shadow
	page.setColor(Color.gray);
	page.fillOval(200,350,160,23);


        //body
        page.setColor(Color.white);
        page.fillOval(230,100,100,100);	// head
	page.fillOval(205,175,150,170);	//torso

	// eyes
	page.setColor(Color.black);
	page.fillOval(250,130,20,20);	//left eye
	page.fillOval(290,130,20,20);	//right eye

	//pupil
	page.setColor(Color.white);
	page.fillOval(252,132,10,10);	//left pupil
	page.fillOval(292,132,10,10);	//right pupil

	//face features
	page.setColor(Color.orange);
	page.fillRect(275,153,100,5);	//nose
	page.fillOval(275,152,80,10);	//nose
    page.setColor(Color.black);
 	page.drawArc(248,122,60,60,235,95);	//mouth

    //hands
    page.setColor(Color.black);
        page.drawLine(220,240,220,270);

        //hat
       page.setColor(Color.black);
        page.fillRect(237,90,90,20);
        page.fillRect(252,30,60,70);


//Snowoman


	//shadow
	page.setColor(Color.gray);
	page.fillOval(400,350,160,23);


        //body
        page.setColor(Color.white);
        page.fillOval(430,100,100,100);	// head
	page.fillOval(405,175,150,170);	//torso
        page.setColor(Color.black);
    page.drawArc(450,195,35,35,225,95);
    page.drawArc(477,195,35,35,225,95);

	// eyes
	page.setColor(Color.black);
	page.fillOval(450,130,20,20);	//left eye
	page.fillOval(490,130,20,20);	//right eye

	//pupil
	page.setColor(Color.white);
	page.fillOval(462,132,10,10);	//left pupil
	page.fillOval(502,132,10,10);	//right pupil

	//face features
	page.setColor(Color.orange);
	page.fillRect(475,153,100,5);	//nose
	page.fillOval(475,152,80,10);	//nose
    page.setColor(Color.black);

     //hands
    page.setColor(Color.black);
        page.drawLine(420,240,420,270);

        //hat
       page.setColor(Color.red);
        page.fillRect(437,90,90,20);
        page.fillRect(452,30,60,70);

    }
}
