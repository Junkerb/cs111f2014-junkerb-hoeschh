//==========================================
//// Honor Code: The work I am submitting is a result of my own thinkings and efforts
// Hunter Hoesch
// CMPSC 111 Fall 2014
// Lab # 4
// Date: 09 25 2014
//
// Purpose: Produce a picture of a Snowman.
//==========================================


import javax.swing.*;

public class ScenarioBDisplay
{
    public static void main(String[] args)
    {
        	JFrame window = new JFrame(" Your name ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new ScenarioB());
          	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
			window.setSize(600, 400);

    }
}

