//=================================================
//Snowman scene 4.
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class ScenarioB extends JApplet {

 public void paint(Graphics page) {
        final int WIDTH = 600;
        final int HEIGHT = 400;

        //fill entire background cyan
        page.setColor(Color.cyan);
        page.fillRect(0,0,WIDTH, HEIGHT);

        //draw sun in corner
        page.setColor(Color.yellow);
     page.fillOval(0,0,50,50);    // Graphics Object #2: Oval
      //draw rays from sun
    page.drawLine(70,30,100,50); // Graphics Object #3: Line
    page.drawLine(30,70,50,100);
    page.drawLine(50,50,100,100);
     page.drawLine(70,10,150,15);
     page.drawLine(10,65,6,130);

	//snow on ground
	page.setColor(Color.white);
	 page.fillRect(0,HEIGHT - 80,WIDTH,HEIGHT);

     //Snowman

	//shadow
	page.setColor(Color.gray);
	page.fillOval(220,350,160,23);


        //body
        page.setColor(Color.white);
        page.fillOval(250,100,100,100);	// head
	page.fillOval(225,175,150,170);	//torso

	// eyes
	page.setColor(Color.black);
	page.fillOval(270,130,20,20);	//left eye
	page.fillOval(310,130,20,20);	//right eye

	//pupil
	page.setColor(Color.white);
	page.fillOval(272,132,10,10);	//left pupil
	page.fillOval(312,132,10,10);	//right pupil

	//face features
	page.setColor(Color.orange);
	page.fillRect(295,153,100,5);	//nose
	page.fillOval(295,152,80,10);	//nose
    page.setColor(Color.black);
 	page.drawLine(325,170,300,170);	//mouth

    //hands
    page.setColor(Color.black);
        page.drawLine(240,240,370,270);

        //hat
       page.setColor(Color.black);
        page.fillRect(257,90,90,20);
        page.fillRect(272,30,60,70);


//Snowoman


	//shadow
	page.setColor(Color.gray);
	page.fillOval(400,350,160,23);


        //body
        page.setColor(Color.white);
        page.fillOval(430,100,100,100);	// head
	page.fillOval(405,175,150,170);	//torso
        page.setColor(Color.black);
    page.drawArc(450,195,35,35,225,95);
    page.drawArc(477,195,35,35,225,95);

	// eyes
	page.setColor(Color.black);
	page.fillOval(450,130,20,20);	//left eye
	page.fillOval(490,130,20,20);	//right eye

	//pupil
	page.setColor(Color.white);
	page.fillOval(457,132,10,10);	//left pupil
	page.fillOval(497,132,10,10);	//right pupil

	//face features
	page.setColor(Color.orange);
	page.fillRect(400,153,100,5);	//nose
	page.fillOval(420,152,80,10);	//nose
    page.setColor(Color.black);
    page.drawArc(440,122,60,60,235,95);//mouth

     //hands
    page.setColor(Color.black);
        page.drawLine(420,240,370,270);

        //hat
       page.setColor(Color.red);
        page.fillRect(437,90,90,20);
        page.fillRect(452,30,60,70);


//Snowman Kid 1

        //shadow
	page.setColor(Color.gray);
	page.fillOval(100,350,90,23);


        //body
        page.setColor(Color.white);
        page.fillOval(120,160,50,50);	// head
	page.fillOval(105,205,80,140);	//torso

	// eyes
	page.setColor(Color.black);
	page.fillOval(130,175,10,10);	//left eye
	page.fillOval(150,175,10,10);	//right eye

	//pupil
	page.setColor(Color.white);
	page.fillOval(131,176,5,5);	//left pupil
	page.fillOval(151,176,5,5);	//right pupil

	//face features
	page.setColor(Color.orange);
	page.fillRect(143,188,30,5);	//nose
	page.fillOval(141,189,30,5);	//nose

    page.setColor(Color.black);
    page.drawArc(120,172,30,30,215,55);//mouth

     //hands
    page.setColor(Color.black);
        page.drawLine(120,240,120,270);

        //hat
       page.setColor(Color.green);
        page.fillRect(121,153,50,10);
        page.fillRect(130,115,30,40);


        //Snowman Kid 2

        //shadow
	page.setColor(Color.gray);
	page.fillOval(10,350,90,23);


        //body
        page.setColor(Color.white);
        page.fillOval(30,160,50,50);	// head
	page.fillOval(15,205,90,140);	//torso

	// eyes
	page.setColor(Color.black);
	page.fillOval(40,175,10,10);	//left eye
	page.fillOval(60,175,10,10);	//right eye

	//pupil
	page.setColor(Color.white);
	page.fillOval(41,176,5,5);	//left pupil
	page.fillOval(61,176,5,5);	//right pupil

	//face features
	page.setColor(Color.orange);
	page.fillRect(53,188,30,5);	//nose
	page.fillOval(51,189,30,5);	//nose

    page.setColor(Color.black);
    page.fillOval(50,195,5,10);//mouth

     //hands
    page.setColor(Color.black);
        page.drawLine(30,240,30,270);

        //hat
       page.setColor(Color.blue);
        page.fillRect(31,153,50,10);
        page.fillRect(40,115,30,40);
    }
}
