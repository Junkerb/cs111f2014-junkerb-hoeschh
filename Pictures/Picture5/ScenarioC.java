//=================================================
//Snowman scene 5.
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class ScenarioC extends JApplet {

 public void paint(Graphics page) {
        final int WIDTH = 600;
        final int HEIGHT = 400;

        //fill entire background cyan
        page.setColor(Color.cyan);
        page.fillRect(0,0,WIDTH, HEIGHT);

        //draw sun in corner
        page.setColor(Color.yellow);
     page.fillOval(0,0,50,50);    // Graphics Object #2: Oval
      //draw rays from sun
    page.drawLine(70,30,100,50); // Graphics Object #3: Line
    page.drawLine(30,70,50,100);
    page.drawLine(50,50,100,100);
     page.drawLine(70,10,150,15);
     page.drawLine(10,65,6,130);

	//grass on ground
	page.setColor(Color.green);
	 page.fillRect(0,HEIGHT - 80,WIDTH,HEIGHT);

   //Snowman


	//shadow
	page.setColor(Color.gray);
	page.fillOval(400,350,160,23);


        //body
        page.setColor(Color.white);
        page.fillOval(430,100,100,100);	// head
	page.fillOval(405,175,160,170);	//torso
        page.setColor(Color.black);

	// eyes
	page.setColor(Color.black);
	page.fillOval(450,130,20,20);	//left eye
	page.fillOval(490,130,20,20);	//right eye

	//pupil
	page.setColor(Color.white);
	page.fillOval(457,132,10,10);	//left pupil
	page.fillOval(497,132,10,10);	//right pupil

	//face features
	page.setColor(Color.orange);
	page.fillRect(400,153,100,5);	//nose
	page
    .fillOval(420,152,80,10);	//nose
    page.setColor(Color.black);
    page.drawArc(440,172,60,60,35,95);//mouth

     //hands
    page.setColor(Color.black);
        page.drawLine(420,240,420,270);

        //hat
       page.setColor(Color.black);
        page.fillRect(437,90,90,20);
        page.fillRect(452,30,60,70);


        //House

        page.setColor(Color.red);
        page.fillRect(50,120,300,200);//base
        page.setColor(Color.black);
        page.fillRect(90,130,60,70);//window
        page.fillRect(250,130,60,70);
        page.fillRect(160,220,50,100);//door
    }
}
